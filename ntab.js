import config from "./config.js";

const absReg=/.*:\/\//

function generateChildren(){
    const children=[]
    for(let title in config){
        if(title=="theme") continue
        let tnode=document.createElement("h1")
        tnode.innerText=title
        let sectionNode=document.createElement("section")
        let ulNode=document.createElement(config[title].listType||"ul")
        for(let bookmark of config[title]){
            if(!bookmark.href) continue
            let liNode=document.createElement("li")
            let aNode=document.createElement("a")
            aNode.innerText=bookmark.name || bookmark.href
            aNode.setAttribute("href", absReg.test(bookmark.href)?bookmark.href:String(new URL("https://"+bookmark.href)))
            liNode.appendChild(aNode)
            ulNode.appendChild(liNode)
        }
        sectionNode.appendChild(tnode)
        sectionNode.appendChild(ulNode)
        children.push(sectionNode)
    }
    return children
}

function main(){
    let body=document.body
    if(config.theme) body.setAttribute("data-theme", config.theme)
    for(let section of generateChildren()){
        body.appendChild(section)
    }
}

window.addEventListener("load", main)
